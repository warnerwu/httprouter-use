package main

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"fmt"
	"github.com/labstack/gommon/log"
	"encoding/json"
)

/**
 * Book 结构体
 */
type Book struct {
	// 本书的主要标识符。这将是独一无二的。
	ISDN string `json:"isdn"`
	Title string `json:"title"`
	Author string `json:"author"`
	Pages uint64 `json:"pages"`
}

/**
 * JSON格式操作成功响应结构体
 */
type JsonSuccessResponse struct {
	// 保留字段，以便向API响应添加一些元信息
	Meta interface{} `json:"meta"`
	Data interface{} `json:"data"`
}

/**
 * JSON格式操作成功响应结构体
 */
type JsonErrorResponse struct {
	Error *ApiError `json:"error"`
}

/**
 * API接口错误
 */
type ApiError struct {
	Code uint32 `json:"code"`
	Message string `json:"message"`
}

/**
 * 响应头结构体
 */
type Headers struct {
	headers map[string]interface{}
}

// 以ISDN为键存储书籍的地图
// 这用作代替实际数据库的存储
var BookStore = make(map[string]*Book)


func main() {
	// httprouter 实例
	router := httprouter.New()

	// 路由注册(首页/)
	router.GET("/", Index)

	// 路由注册(书籍列表页/books)
	router.GET("/books", BookIndex)

	// 路由注册(书籍详情页/books/:isdn)
	router.GET("/books/:isdn", BookDetail)

	// 创建几个示例书条目
	BookStore["isdn_i_love_go_web"] = &Book{
		ISDN: "isdn_i_love_go_web",
		Title: "Go Web",
		Author: "astaxie",
		Pages: 550,
	}

	BookStore["isdn_i_love_php_web"] = &Book{
		ISDN: "isdn_i_love_php_web",
		Title: "PHP Web",
		Author: "张三",
		Pages: 666,
	}

	// 运行服务并监听
	log.Fatal(http.ListenAndServe(":8297", router))
}

/**
 * 首页
 */
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome\n")
}

/**
 * 设置响应头信息
 */
func setHeader(w http.ResponseWriter, headers map[string]string, httpStatus int) http.ResponseWriter {
	// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	for key, value := range headers {
		w.Header().Set(key, value)
	}

	// 设置响应状态码
	if httpStatus != 0 {
		w.WriteHeader(httpStatus)
	}

	// 返回响应
	return w
}

/**
 * 书籍处理程序列表操作
 * GET /books
 */
func BookIndex(w http.ResponseWriter, r *http.Request, _ httprouter.Params)  {
	// 切片数组初始化
	var books []*Book

	// 循环追加结构体对象到切片数组
	for _, book := range BookStore {
		books = append(books, book)
	}

	// 构建响应数据结构体 - 成功
	successResponse := &JsonSuccessResponse{Data: books}

	// 设置头信息
	w = setHeader(w, map[string]string{"Content-Type": "application/json; charset=UTF-8"}, http.StatusOK)

	// 生成响应JSON数据, 并返回给客户端进行渲染
	ResponseRender(w, successResponse)
}

/**
 * 书籍处理程序显示某一本书籍动作
 * GET /books/:isdn
 */
func BookDetail(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	// 获取GET请求参数
	isdn := params.ByName("isdn")

	// 获取书籍切片资源元素结构体对象
	book, ok := BookStore[isdn]

	// 未获取到时
	if !ok {
		// 设置头信息
		w = setHeader(w, map[string]string{"Content-Type": "application/json; charset=UTF-8"}, http.StatusNotFound)

		// 构建响应数据结构体 - 错误
		errorResponse := JsonErrorResponse{Error: &ApiError{Code: http.StatusNotFound, Message: "Resource Not Found"}}

		// 生成响应JSON数据, 并返回给客户端进行渲染
		ResponseRender(w, errorResponse)
	}

	// 构建响应数据结构体 - 成功
	errorResponse := JsonSuccessResponse{Data: book}

	// 生成响应JSON数据, 并返回给客户端进行渲染
	ResponseRender(w, errorResponse)
}

/**
 * 响应数据渲染
 */
func ResponseRender(w http.ResponseWriter, responseDate interface{}) {
	// 生成响应JSON数据
	if err := json.NewEncoder(w).Encode(responseDate); err != nil {
		panic(err)
	}
}

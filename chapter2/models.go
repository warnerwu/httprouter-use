package main

/**
 * Book 结构体数据模型
 */
type Book struct {
	// 本书的主要标识符。这将是独一无二的。
	ISDN string `json:"isdn"`
	Title string `json:"title"`
	Author string `json:"author"`
	Pages uint64 `json:"pages"`
}

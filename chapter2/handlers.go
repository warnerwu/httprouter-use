package main

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"fmt"
)

/**
 * 书籍处理程序列表操作
 * GET /books
 */
func BookIndex(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// 切片数组初始化
	var books []*Book

	// 循环追加结构体对象到切片数组
	for _, book := range BookStore {
		books = append(books, book)
	}

	// 构建响应数据结构体 - 成功
	successResponse := &JsonSuccessResponse{Data: books}

	// 设置头信息
	w = setHeader(w, map[string]string{"Content-Type": "application/json; charset=UTF-8"}, http.StatusOK)

	// 生成响应JSON数据, 并返回给客户端进行渲染
	ResponseRender(w, successResponse)

	// 直接返回
	return
}

/**
 * 书籍处理程序显示某一本书籍动作
 * GET /books/:isdn
 */
func BookDetail(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	// 获取GET请求参数
	isdn := params.ByName("isdn")

	// 获取书籍切片资源元素结构体对象
	book, ok := BookStore[isdn]

	// 未获取到时
	if !ok {
		// 设置头信息
		w = setHeader(w, map[string]string{"Content-Type": "application/json; charset=UTF-8"}, http.StatusNotFound)

		// 构建响应数据结构体 - 错误
		errorResponse := JsonErrorResponse{Error: &ApiError{Code: http.StatusNotFound, Message: "Resource Not Found"}}

		// 生成响应JSON数据, 并返回给客户端进行渲染
		ResponseRender(w, errorResponse)

		// 直接返回
		return
	}

	// 构建响应数据结构体 - 成功
	errorResponse := JsonSuccessResponse{Data: book}

	// 生成响应JSON数据, 并返回给客户端进行渲染
	ResponseRender(w, errorResponse)

	// 直接返回
	return
}

/**
 * 首页
 */
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome\n")
}

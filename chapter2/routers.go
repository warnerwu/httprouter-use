package main

import "github.com/julienschmidt/httprouter"

/**
 * 路由注册函数
 */
func getRouters() *httprouter.Router {
	// httprouter 实例
	router := httprouter.New()

	// 路由注册(首页/)
	router.GET("/", Index)

	// 路由注册(书籍列表页/books)
	router.GET("/books", BookIndex)

	// 路由注册(书籍详情页/books/:isdn)
	router.GET("/books/:isdn", BookDetail)

	// 返回路由
	return router
}
package main

import (
	"net/http"
	"encoding/json"
)

/**
 * JSON格式操作成功响应结构体
 */
type JsonSuccessResponse struct {
	// 保留字段，以便向API响应添加一些元信息
	Meta interface{} `json:"meta"`
	Data interface{} `json:"data"`
}

/**
 * JSON格式操作成功响应结构体
 */
type JsonErrorResponse struct {
	Error *ApiError `json:"error"`
}

/**
 * 响应头结构体
 */
type Headers struct {
	headers map[string]interface{}
}

/**
 * API接口错误
 */
type ApiError struct {
	Code uint32 `json:"code"`
	Message string `json:"message"`
}

/**
 * 响应数据渲染
 */
func ResponseRender(w http.ResponseWriter, responseDate interface{}) {
	// 生成响应JSON数据
	if err := json.NewEncoder(w).Encode(responseDate); err != nil {
		panic(err)
	}
}

/**
 * 设置响应头信息
 */
func setHeader(w http.ResponseWriter, headers map[string]string, httpStatus int) http.ResponseWriter {
	// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	for key, value := range headers {
		w.Header().Set(key, value)
	}

	// 设置响应状态码
	if httpStatus != 0 {
		w.WriteHeader(httpStatus)
	}

	// 返回响应
	return w
}



package main

import (
	"net/http"
	"log"
)

func main() {
	routers := getRouters()

	// 运行服务并监听
	log.Fatal(http.ListenAndServe(":8297", routers))
}

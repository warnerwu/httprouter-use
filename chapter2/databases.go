package main

// 以ISDN为键存储书籍的地图
// 这用作代替实际数据库的存储
var BookStore = make(map[string]*Book)

/**
 * 初始化数据
 */
func init() {
	// 创建几个示例书条目
	BookStore["isdn_i_love_go_web"] = &Book{
		ISDN: "isdn_i_love_go_web",
		Title: "Go Web",
		Author: "astaxie",
		Pages: 550,
	}

	BookStore["isdn_i_love_php_web"] = &Book{
		ISDN: "isdn_i_love_php_web",
		Title: "PHP Web",
		Author: "张三",
		Pages: 666,
	}
}
